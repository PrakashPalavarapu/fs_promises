const fsp = require("fs").promises;

function lipsumFile() {
  fsp
    .readFile("./lipsum_1.txt", "utf-8")
    .then((data) => {
      return data.toUpperCase();
    })
    .then((upperData) => {
      let upperFileName = "./upperLipsum.txt";
      fsp.writeFile(upperFileName, JSON.stringify(upperData));
      console.log(`created file ${upperFileName}`);
      return [upperFileName, upperData];
    })
    .then(([upperFileName, upperData]) => {
      fsp.writeFile("fileNames.txt", JSON.stringify(upperFileName) + "\n");
      console.log(`appended ${upperFileName} to fileNames`);
      let lowerSentences = upperData.toLowerCase().split(". ");
      return lowerSentences;
    })
    .then((lowerSentences) => {
      let lowerSentencesFileName = "./lowerSentences.txt";
      fsp.writeFile(lowerSentencesFileName, JSON.stringify(lowerSentences));
      console.log(`created ${lowerSentencesFileName}`);
      return [lowerSentencesFileName, lowerSentences];
    })
    .then(([lowerSentencesFileName, lowerSentences]) => {
      fsp.appendFile(
        "./fileNames.txt",
        JSON.stringify(lowerSentencesFileName) + "\n"
      );
      console.log(`appended ${lowerSentencesFileName}`);
      let sortedLower = lowerSentences.sort();
      return sortedLower;
    })
    .then((sortedLower) => {
      let sortedLowerFileName = "./sortedLowerFileName";
      fsp.writeFile(sortedLowerFileName, JSON.stringify(sortedLower));
      console.log(`created file ${sortedLowerFileName}`);
      return [sortedLowerFileName, sortedLower];
    })
    .then(([sortedLowerFileName, sortedLower]) => {
      fsp.appendFile(
        "fileNames.txt",
        JSON.stringify(sortedLowerFileName) + "\n"
      );
      console.log(`appended ${sortedLowerFileName}`);
      return fsp.readFile("./fileNames.txt", "utf-8");
    })
    .then((fileNamesData) => {
      let files = fileNamesData.split("\n");
      for (let file of files) {
        if (file != "") {
          fsp.unlink(file.slice(1, file.length - 1));
          console.log(`file ${file} deleted`);
        }
      }
    })
    .catch((err) => {
      console.log(err.name, err.message);
    });
}
module.exports = lipsumFile;
