const fsp = require("fs").promises;

function createJSONfiles() {
  let files = [
    "json1.json",
    "json2.json",
    "json3.json",
    "json4.json",
    "json5.json",
  ];
  fsp
    .mkdir(`./JSONfiles/`, { recursive: true })
    .then(() => {
      console.log(`created folder JSONfiles`);
      return files;
    })
    .then((files) => {
      // console.log(files);
      fsWritePromises = [];
      for (let index = 0; index < 5; index++) {
        fsWritePromises.push(fsp.writeFile(`./JSONfiles/${files[index]}`, ""));
      }
      return Promise.allSettled(fsWritePromises);
    })
    .then((creationResult) => {
      fsDeletePromises = [];
      for (let index in creationResult) {
        if (creationResult[index].status == "fulfilled") {
          console.log(`file ${files[index]} created successfully`);
          fsDeletePromises.push(fsp.unlink(`./JSONfiles/${files[index]}`));
        }
      }
      return Promise.allSettled(fsDeletePromises);
    })
    .then((fsDeletePromises) => {
      for (let index in fsDeletePromises) {
        if (fsDeletePromises[index].status == "fulfilled") {
          console.log(`${files[index]} deleted`);
        }
      }
    })
    .catch((err) => {
      console.error(err.name, err.message);
    });
}

module.exports = createJSONfiles;

/* Another approach, This approach creates a promise hell */
// function createJSONfiles1() {
//   fsp
//     .mkdir(`./JSONfiles/`, { recursive: true })
//     .then(() => {
//       console.log("created JSONfiles folder");
//     })
//     .then(() => {
//       for (let index = 0; index <= 4; index++) {
//         fsp
//           .writeFile(
//             `./JSONfiles/json${index}.json`,
//             JSON.stringify(`this is json${index} file`),
//             "utf-8"
//           )
//           .then(() => {
//             console.log(`created json${index}.json`);
//           })
//           .then(() => {
//             fsp.unlink(`./JSONfiles/json${index}.json`).then(() => {
//               console.log(`json${index}.json deleted`);
//             });
//           });
//       }
//     })
//     .catch((err) => {
//       console.log(err.name, err.message);
//     });
// }
